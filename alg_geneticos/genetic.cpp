// genetic.cpp
#include <iostream> 
#include <cstdlib> 
#include <ctime> 
#include <cmath> 
using namespace std;
// for srand, rand // for cos, sin, pow
// fitness function where maximum to be found 
double f(double x)
{ 
 return cos(x) - sin(2*x); 
}

// fitness function value for individual
double f_value(double (*func)(double), int* arr, int& N, double a,double b)
{
 double res;
 double m = 0.0; 
 for(int j=0;j<N;j++)
 {
  double k = j;
  m+= arr[N-j-1]*pow(2.0,k);
 }
 double x = a + m*(b-a)/(pow(2.0,N)-1.0); 
 res = func(x);
 return res;
}

// x_value at global maximum
double x_value(int* arr, int& N,double a,double b) 
{
 double m = 0.0;
 for(int j=0;j<N;j++)
 {
  double k = j;
  m += arr[N-j-1]*pow(2.0,k);
 }
 double x = a + m*(b-a)/(pow(2.0,N)-1.0); 
 return x;
}

// setup the population (farm) 
void setup(int** farm,int M, int N) 
{
 srand((unsigned long) time(NULL)); 
 for(int j=0;j<M;j++)
  for(int k=0;k<N; k++)
   farm[j][k] = rand()%2;
}

// cross two individuals
void crossings(int** farm, int& M, int& N, double &a, double& b) 
{
 int K = 2;
 int** temp = new int* [K];
 for(int i=0;i<K;i++) 
  temp[i] = new int[N];

 double res[4];
 int r1 = rand()%M; 
 int r2 = rand()%M; 
 // random returns a value between
 // 0 and one less than its parameter 
 while(r2 == r1) 
  r2 = rand()%M;
 res[0] = f_value(f, farm[r1], N, a, b); 
 res[1] = f_value(f, farm[r2], N, a, b);

for(int j=0;j<N;j++)
{
 temp[0][j] = farm[r1] [j] ; 
 temp[1][j] = farm[r2] [j] ; 
}

 int r3 = rand()%(N-2) + 1;
 for(int j=r3;j<N;j++)
 {
  temp[0][j] = farm[r2][j]; 
  temp[1][j] = farm[r1][j] ;
 }
  res[2] = f_value(f ,temp[0] ,N,a,b); 
  res[3] = f_value(f ,temp[1] ,N,a,b);
  if(res[2] > res[0])
  {
   for(int j=0;j<N;j++)
    farm[r1] [j] = temp[0] [j] ; 
    res[0] = res [2] ;
  }

  if(res[3] >res[1])
  {
   for(int j=0;j<N;j++) 
    farm[r2][j] = temp[1] [j] ; 
   res[1] =res[3];
  }

  for(int j=0;j<K;j++)
   delete[] temp[j] ; 
   delete[] temp;
}

// mutate an individual
void mutate(int** farm, int& M, int& N, double& a,double& b) 
{
  double res[2];
  int r4=rand()%N; 
  int r1=rand()%M; 
  res[0] = f_value(f ,farm[r1] ,N,a,b);
  int v1 = farm[r1] [r4];

  if(v1 == 0) farm[r1][r4] = 1;
  if(v1 == 1) farm[r1][r4] = 0;

  double a1 = f_value(f, farm[r1], N,a,b); 
  if(a1 < res[0]) 
   farm[r1][r4] = v1;

  int r5 = rand()%N; 
  int r2 = rand()%M; 
  res[1] = f_value(f, farm[r2] ,N,a,b); 
  int v2 = farm[r2][r5];
  if(v2 == 0) farm[r2][r5] = 1;
  if(v2 == 1) farm[r2][r5] = 0;
  double a2 = f_value(f, farm[r2], N,a,b); 
  if(a2 < res[1]) farm[r2][r5] = v2;
}

int main(void)
{
 int M = 12; // population (farm) has 12 individuals (animals) 
 int N = 10; // length of binary string

 int** farm = new int*[M]; // allocate memory for population 
 for(int i=0;i<M;i++) { farm[i] = new int[N]; }

 setup(farm,M,N);
 double a=0.0,b=6.28318; //interval[a,b]

 for(int k=0;k<1000;k++)
 {
  crossings(farm,M,N,a,b); 
  mutate(farm,M,N,a,b);
 } //endforloop

 for (int j=0;j<N;j++)
  cout << "farm[l] [" << j << "] = " << farm[1] [j] << endl; 
  cout << endl;

 for(int j=0;j<M;j++)
  cout << "fitness f_value[" << j << "] = "
   << f_value(f ,farm[j] ,N,a,b)
   << " " << "x_value[" << j << "]=" << x_value(farm[j] ,N,a,b) << endl;

 for(int j=0;j<M;j++) 
  delete[] farm[j]; 
  delete[] farm; 
  return 0;
}
