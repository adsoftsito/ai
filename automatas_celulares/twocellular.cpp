#include <iostream> 
#include <cstdlib>
using namespace std;

int neighbours(int a, int b, int c, int d, int e, int f, int g, int h)
{
 int result =a+b+c+d+e+f+g+h;
 return result; 
}

int main(void)
{
 int i, j;
 int N = 32; // size of the grid
 // allocating memory for N x N array 
 int** A = new int* [N]; 

 for(i=0;i<N;i++)
  A[i] = new int[N] ; 

  int** B = new int* [N]; 

  for(i=0;i<N;i++)
   B[i] = new int[N] ;
  
  // initial configuration 
  cout << "Initial State " << endl;
  for(i=0;i<N;i++)
  { 
   for(j=0;j<N;j++) 
   {
    A[i][j] = rand() % 2;
   }
  }
  // set the following grid points alive
/*
  A[0][0] =1;
  A[1][2] =1;
  A[2][0] =1;
  A[2][2] =1;

  A[1][3] =1; 
  A[1][4] =1; 
  A[2][1] =1; 
  A[2][4] =1; 

  A[3][0] =1;
  A[3][2] =1;
  A[4][1] =1;
  A[4][4] =1;

  A[5][0] =1;
  A[5][2] =1;
  A[6][1] =1;
  A[6][3] =1;
*/  
  for (i=0; i<N; i++)
  {
   for(j=0; j<N; j++)
   {
    cout << A[i][j] << " ";
   }
    cout << endl;
  }
  // iterations

  int T = 100; // number of iterations 
  for(int t=0;t<T;t++)
  {


   for(i=0;i<N;i++)
   {
    for(j=0;j<N;j++)
    {
     B[i][j] = A[i][j];
    }
   }
 

  for(i=0;i<N;i++)
   {
    for(j=0;j<N;j++)
    {
     int temp=
     neighbours(
     B[((i+1)%N)][j],
     B[((i+1)%N)][((j+1)%N)],
     B[i][((j+1)%N)],
     B[((i-1+N)%N)][((j+1)%N)],
     B[((i-1+N)%N)][j], 
     B[((i-1+N)%N)][((j-1+N)%N)] ,
     B[i][((j-1+N)%N)] , 
     B[((i+1)%N)][((j-1+N)%N)]);

     // rule 1
     if((B[i][j] == 0) && (temp == 3)) 
     { 
      A[i][j] = 1; 
     }

     // rule 2
     if((B[i][j] == 1) && ((temp ==2) || (temp == 3))) 
     {
      A[i][j] = 1; 
     }

     // rule 3
     if((B[i][j] == 1) && ((temp >= 4) || (temp <= 1)))
     {
      A[i][j] = 0 ; 
     }

  }
 }

cout << endl;
cout << "t = " << t; 
cout << endl;

// output foreach time step 


for(i=0;i<N;i++)
{
 for(j=0;j<N;j++)
 {
  cout <<  A[i][j] << " "; 
 }
 cout << endl;
}
  cout << endl << endl;
} //endfor loop t

// free the memory
for(i=0;i<N;i++) 
 delete[] A[i];

delete[] A;


for(i=0;i<N;i++) 
 delete[] B[i]; 

delete[] B;

return 0;


}
