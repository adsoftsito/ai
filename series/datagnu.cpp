#include <fstream> // for ofstream, close 
using namespace std;
int main(void)
{
ofstream data("timeev.dat");
unsigned long T = 100;	// number of iterations 
double x0 = 0.618;	// initial value 
double x1;

for(unsigned long t=0;t<T;t++)
 { 
	x1 = 4.0*x0*(1.0 - x0); 
	data <<  t <<  " " << x0 << "\n";
	x0 = x1;
} 

data.close(); 
return 0;
}

