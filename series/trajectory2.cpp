//  trajectory2.cpp
#include <iostream> 
#include <fstream>

#include "verylong.h" //fordatatypeVerylong 
#include "rational.h" //fordatatypeRational 
using namespace std;

inline void map(Rational<Verylong>& x) 
{
 Rational<Verylong> one(1, 1); //number1 
 Rational<Verylong> four(4,1); // number 4 
 Rational<Verylong> x1 = four*x*(one - x); 
 x = x1;
}

int main(void)
{

Rational<Verylong> xO(1,3); // initial value 1/3 
unsigned long T = 10; // number of iterations 
Rational<Verylong> x=xO;

cout<< "x[0]="<< x << endl;
ofstream data("timeev.dat");

for(unsigned long t=0;t<T;t++) 
{ 
	map(x); 
	cout<< "x["<< t+1<< "]="<< x<< endl;
	data << t << " " << x << "\n";
 }
return 0;
}
