// trajectory3.cpp
#include <iostream> 
#include <fstream>

using namespace std;

inline void map(double& x)
{

double x1 = 4.0 * x * (1.0 - x);
x = x1;

}
int main(void)
{

double x0=1.0/3.0;    //initialvalue
unsigned long T = 100; // number of iterations 
double x = x0; 

cout << "x[0] =" << x << endl;
ofstream data("timeev.dat");

for(unsigned long t=0;t<T;t++)
{ 
  map(x);
  cout << "x[" << t+1 << "]=" << x << endl;
  data << t << " " << x << "\n";
}
return 0;
}
